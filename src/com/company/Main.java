package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Manager manager = new Manager(); // j'instancie mon objet Manager, le contenu de son constructeur se lance qu'une seule fois par execution du programme.
        manager.start(); // en plus de son constructeur, j'apelle la méthode start et surtout, je pourrais la rappeler au besoin.
    }
}
