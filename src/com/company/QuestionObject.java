package com.company;

class QuestionObject {
    private String answerText;
    private String questionText;

    QuestionObject(String question, String answer) {
        questionText = question;
        answerText = answer;
    }

    public String getQuestionText() {
        return questionText;
    }
    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }
    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }
    public String getAnswerText() {
        return answerText;
    }

    public boolean checkAnswer(String userAnswer) {
        if (answerText.toLowerCase().equals(userAnswer.toLowerCase())) {
            return true;
        } else {
            return false;
        }
    }
}
