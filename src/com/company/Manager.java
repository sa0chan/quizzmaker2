package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

class Manager { //Class Manager qui va gérer les interactions client
    ArrayList<QuestionObject> questionsList = new ArrayList<QuestionObject>(); // création de la liste vide de l'objet : QuestionObject - Ligne 110
    Scanner sc = new Scanner(System.in); //Instanciation du scanner pour les inputs clients.
    boolean isReadyToStartGame = false; // jé défini 2 boolean qui vont me permettre de gerer la fin de mes boucles
    boolean isReadyToQuit = false;
    UserObject currentUser = new UserObject();

    Manager() { // Constructeur utilisé pour créer le fichier de sauvegarde des questions s'il n'est pas déjà présent.
        try {
            File file = new File("liste de questions.txt"); // instanciation d'un objet File
            file.createNewFile(); // création du document texte
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void start() { // premiere méthode du Manager qui est éxécutée au démarage dans Main
        System.out.println("Quel est votre nom ?");
        currentUser.setUserName(sc.nextLine());
        currentUser.setUserScore(0);
        questionsListUpdater();

        /*
        while (!isReadyToStartGame) { //boucle mise en place pour ajouter plusieurs questions jusqu'a ce que isReadyToStartGame soit True
            questionsListUpdater(); // Je met à jour la liste de questions au cas ou le fichier texte contienne déjà des questions
            System.out.println("Bien le bonjour " + currentUser.getUserName() + "  voulez vous ajouter une question y/n");
            if (sc.nextLine().equals("y")) { // je met le sc.nextline directement dans le If pour Optimiser le code
                saveQuestionsOnFile(addQuestion()); // Je lance la méthode de création de question DANS la méthode de sauvegarde des questions sur le fichier.
            } else {
                if (questionsList.size() == 0) { // si la taille de la liste après avoir lancé questionsListUpdater(); est vide
                    System.out.println("Il n'y à pas encore de question");
                    saveQuestionsOnFile(addQuestion()); // je force la création de question
                } else {
                    isReadyToStartGame = true; // si on ne veux pas faire de question et qu'il y en a déjà, la boucle ne se relance pas
                }
            }
        }
         */
        startGame(); // Suite à la fin de la boucle on lance a prochaine étape , la méthode startGame() - Ligne 56
    }

    private QuestionObject addQuestion() { // Simple méthode de type QuestionObject qui construit la question via les inputs clients
        System.out.println("Rentrez votre question : ");
        String question = sc.nextLine();
        System.out.println("Rentrez votre réponse : ");
        String answer = sc.nextLine();
        return new QuestionObject(question, answer); // je retourne directement un QuestionObject en utilisant son constructeur
    }

    private void startGame() {
        int score = 0;
        for (QuestionObject q : questionsList) {
            System.out.println(q.getQuestionText());
            if(sc.nextLine().toLowerCase().equals(q.getAnswerText().toLowerCase())){
                score++;
                currentUser.setUserScore(score);
            }

        }
        if(score == 3){
            System.out.println("Bravo, " + currentUser.getUserName() +  "! Votre score est " + currentUser.getUserScore() + ".");
        }
        else{
            System.out.println("Ca n'est pas fameux, " + currentUser.getUserName() +  ", votre score est " + currentUser.getUserScore() + "...");
        }
    }

    /*
    private void startGame() { // lancement du questionnaire
        questionsListUpdater(); // suite au possible nouvelles questions, on met à jour notre liste
        Random rand = new Random(); // instanciation d'un random pour choisir la question dans la liste - Ligne 61
       // System.out.println("---Début du questionnaire---");
        while (!isReadyToQuit) {//boucle mise en place pour ajouter plusieurs tentatives jusqu'a ce que isReadyToQuit soit True
            QuestionObject askedQuestion = questionsList.get(rand.nextInt(questionsList.size())); //Selection d'une question dans la liste via un numéro random entre 0 et la longueur de la liste
            System.out.println(askedQuestion.getQuestionText());
           // if (sc.nextLine().toLowerCase().equals(askedQuestion.answerText.toLowerCase())) { // je met dans en condition l'input client comparé a la réponse de l'objet QuestionObject séléctioné
            if(askedQuestion.checkAnswer(sc.nextLine())){
                System.out.println("Bravo !!! voulez vous recommencer ? y/n");
                int currentScore  = currentUser.getUserScore() + 1;
                currentUser.setUserScore(currentScore);
                if (sc.nextLine().equals("n")) {
                    isReadyToQuit = true; // fin de la boucle = --Fin du programme--
                }
            } else {
                System.out.println("raté , la réponse était : ? " + askedQuestion.getAnswerText());
                System.out.println("voulez vous recommencer ? y/n");
                if (sc.nextLine().equals("n")) {
                    isReadyToQuit = true;// fin de la boucle = --Fin du programme--
                    System.out.println("Gros noob t'a que : " + currentUser.getUserScore() + " points !");
                }
            }
        }
    }
    */



    private void saveQuestionsOnFile(QuestionObject question) { // méthode d'écriture sur fichier texte avec en argument une QuestionObject
        try (FileWriter fw = new FileWriter("liste de questions.txt", true);
             BufferedWriter bw = new BufferedWriter(fw);
             PrintWriter out = new PrintWriter(bw)) {
            out.println(question.getQuestionText());
            out.println(question.getAnswerText());
        } catch (IOException e) {
            //exception handling left as an exercise for the reader
        }
    }

    private void questionsListUpdater() { // méthode de mise à jour de la liste de QuestionObject en "lisant" le fichier texte
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader("liste de questions.txt"));
            String line = "";
            while (line != null) {
                line = reader.readLine();
                String line2 = reader.readLine();
                if (line != null || line2 != null) {
                    questionsList.add(new QuestionObject(line, line2));
                }
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
