package com.company;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

import static java.lang.System.lineSeparator;

class JunitGlobalTest {

    private InputStream systemInBackup;
    private PrintStream systemOutBackup;
    private ByteArrayOutputStream systemOutMock;

    @BeforeEach
    void setUp() {
        this.systemInBackup = System.in;
        this.systemOutBackup = System.out;

        this.systemOutMock = new ByteArrayOutputStream();
        System.setOut(new PrintStream(systemOutMock));
    }

    @AfterEach
    void tearDown() {
        System.setIn(systemInBackup);
        System.setOut(systemOutBackup);
    }

    @Test
    void shouldHaveScoreOf0WhenAllAnswersAreIncorrect() {
        this.fakeSystemIn("Norbert" + lineSeparator() +
                "Rouge" + lineSeparator() +
                "Mikou" + lineSeparator() +
                "Asterix" + lineSeparator()
        );

        // à remplacer par le nom de votre Main class
        Main.main(new String[]{});

        Assertions.assertEquals("Quel est votre nom ?" +lineSeparator()+
                "Quel est la couleur du cheval blanc d'Henri IV ?" +lineSeparator()+
                "Comment s'appelle le chien de Tintin ?" +lineSeparator()+
                "Comment s'appelle l'humain d'idéfix ?" +lineSeparator()+
                "Ca n'est pas fameux, Norbert, votre score est 0..."+lineSeparator(), this.systemOutMock.toString());
    }

    @Test
    void shouldHavePerfectScoreWhenAllAnswersAreIncorrect() {
        this.fakeSystemIn("Jacquemine" + lineSeparator() +
                "Blanc" + lineSeparator() +
                "Milou" + lineSeparator() +
                "Obélix" + lineSeparator()
        );

        // à remplacer par le nom de votre Main class
        Main.main(new String[]{});

        Assertions.assertEquals("Quel est votre nom ?" +lineSeparator()+
                "Quel est la couleur du cheval blanc d'Henri IV ?" +lineSeparator()+
                "Comment s'appelle le chien de Tintin ?" +lineSeparator()+
                "Comment s'appelle l'humain d'idéfix ?" +lineSeparator()+
                "Bravo, Jacquemine! Votre score est 3."+lineSeparator(), this.systemOutMock.toString());
    }

    private void fakeSystemIn(String userInput) {
        InputStream systemInMock = new ByteArrayInputStream(userInput.getBytes());
        System.setIn(systemInMock);
    }
}

